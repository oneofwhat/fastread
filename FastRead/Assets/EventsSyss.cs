﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // Required when Using UI elements.

public class EventsSyss : MonoBehaviour
{
    public InputField mainSlider;
    public InputField textSlider;
    public Input inputPointer;
    public RectTransform textHeight;

    public void Start()
    {
        //Adds a listener to the main slider and invokes a method when the value changes.
        mainSlider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        textSlider.onValueChanged.AddListener(delegate { ValueChangeTextHeight(); });
    }

    // Invoked when the value of the slider changes.
    public void ValueChangeTextHeight()
    {
        int outPerform;
        int.TryParse(textSlider.text, out outPerform ) ;
        textHeight.sizeDelta = new Vector2(9999, outPerform);
    }
    public void ValueChangeCheck()
    {
        float.TryParse(mainSlider.text,out inputPointer.counter);
    }
}