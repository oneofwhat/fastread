﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Input : MonoBehaviour {

    public InputField mainInputField;
    public Text outPutText;
    public bool readAble = true;
    public void Start()
    {
        mainInputField.text = ReadString();
        
    }
    string  ReadString()
    {
        string path = "Assets/test.txt";

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        string s= reader.ReadToEnd();
        reader.Close();
        return s;
    }
    string[] strArr;
    int wordIterator, pausedIter=0;
    private float startTime;
   
    public float counter;
    public void setTimer(string time)
    {
        bool rand = float.TryParse(time,out counter);
    }

    public bool started=false;
    public void StartTextMagic()
    {
        if (started)
        {
            StopAllCoroutines();
            pausedIter = wordIterator;
            
            started = false;
            return;
        }
        started = true;
        wordIterator =  1;
        startTime = Time.time;
        strArr = mainInputField.text.Split(' ');

        outPutText.text = strArr[0];
        StartCoroutine(WaitAndChange());

        started = false;
    }
    IEnumerator WaitAndChange()
    {
        yield return new WaitForSeconds(counter);
        outPutText.text = strArr[wordIterator];
        if (outPutText.text !=  ""  &&char.IsDigit(strArr[wordIterator][0]))
        {
            yield return new WaitForSeconds(counter*2.333f);
        }
        if (wordIterator == strArr.Length - 1)
        {
            started = false;
        }
        wordIterator += 1;
        
        if (wordIterator < strArr.Length)
        {
            StartCoroutine(WaitAndChange());
        }       
    }
}
